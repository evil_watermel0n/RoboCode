# RoboCode

This AI is a little robot, fighting against other robots in an arena.

Every programmer get a skeletal structure and fill it with the API from the Robocode founders.
I uploaded my robot, if you want an example maybe for your own robot or just want to see how it works.

You can download everything you need on http://robocode.sourceforge.net/
